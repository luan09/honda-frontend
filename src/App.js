import Home from './Home'
import "./app.scss";
import Header from './Components/header';
import Footer from './Components/footer';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <div className="App">
     <Router>
        <Header />
        <Home />
        <Footer />
     </Router>
    </div>
  );
}

export default App;
