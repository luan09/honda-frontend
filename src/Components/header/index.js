import React, { useState, useEffect } from 'react';
import { HashLink as Link }  from 'react-router-hash-link';

import '../../Styles/Components/_header.scss';

import homeIcon from '../../assets/images/header/home-button.svg';
import envelopeIcon from '../../assets/images/header/black-envelope.svg';
import localizacao from '../../assets/images/header/localizacao.svg';



const Header = () => {
  const [flags, setFlags] = useState(true);

 const showFlags = () => {
    if (window.innerWidth <= 768) {
      setFlags(false);
    } else {
      setFlags(true);
    }
  };

  useEffect(() => {
    showFlags();
  }, []);

  window.addEventListener("resize", showFlags);


  return(
    <div id="header">
      {flags ?
        <section className="desktop">
          <div className="icons-desktop">
           <Link smooth className="link" to="#section-1"> 
            <div className="icon-frase">
              <img src={homeIcon} alt="" />
              <h6>Ir para o Portal Banzai</h6>
            </div>
          </Link>
          <div className="loca-env">
            <Link smooth className="link" to="#section-8">
            <div className="icon-frase">
              <img src={localizacao} alt="" />
              <h6>Encontre uma concessionária</h6>
            </div>
            </Link>
            <Link smooth className="link" to="#section-8">
              <div className="icon-frase">
                <img src={envelopeIcon} alt="" />
                <h6>Fale Conosco</h6>
              </div>
            </Link>
          </div>
          </div>
        </section>
      :
      <section className="mobile">
        <div className="icons">
          <Link smooth className="link" to="#section-1"><img src={homeIcon} alt="" /></Link>
          <Link smooth className="link" to="#section-8"><img src={localizacao} alt="" /></Link>
          <Link smooth className="link" to="#section-8"><img src={envelopeIcon} alt="" /></Link>
        </div>
      </section>
      }
    </div>
  );
}


export default Header;