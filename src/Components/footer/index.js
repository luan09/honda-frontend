import React, { useState, useEffect } from 'react';

import '../../Styles/Components/_footer.scss';

import logoIbama from '../../assets/images/footer/logo_ibama.png';
import logoMobile from '../../assets/images/footer/logo-mobile.png';
import logo from '../../assets/images/footer/logo.png';


const Footer = () =>{

  const [flags, setFlags] = useState(true);

  const showFlags = () => {
     if (window.innerWidth <= 550) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="footer">
      <section className="mobile-desktop">
       <img className="logo" src={flags ? logo :logoMobile} alt ="" />

       <div className="ibama">
         <h6>Todos juntos fazem um trânsito melhor</h6>
         <img className="logo-ibama" src={logoIbama} alt="" />
       </div>
      </section>

    </div>
  );
}

export default Footer;