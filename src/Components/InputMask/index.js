import React, { useRef, useEffect, useState } from 'react';
import ReactInputMask, { Props as InputProps } from 'react-input-mask';

import '../Input/styles.scss';

import { useField } from '@unform/core';

export default function InputMask({ name,icon, ...rest }) {
  const inputRef = useRef(null);
  const { fieldName, registerField, defaultValue, error } = useField(name);
  const [mask, setMask] = useState(defaultValue);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      setValue(ref, value) {
        ref.setInputValue(value);
      },
      clearValue(ref) {
        ref.setInputValue('');
      },
    });
  }, [fieldName, registerField]);

  function handleMask(e) {
    const { value } = e.target;
    return setMask(value);
  }

  return (
    <div id='input'>
      <div className='form-group'>
        <ReactInputMask
          className='form-control'
          mask={mask}
          onChange={(e) => handleMask(e)}
          defaultValue={defaultValue}
          ref={inputRef}
          {...rest}
        />
        <img className="icon" src={icon} alt="" />
        {error && <span className='error'> {error}</span>}
      </div>
    </div>
  );
}
