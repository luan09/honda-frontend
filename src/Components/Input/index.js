import React, { useEffect, useRef, useState } from 'react';
import { useField } from '@unform/core';

import './styles.scss';

const Input = ({ name,icon, ...rest }) => {
  const inputRef = useRef(null);
  const { registerField, fieldName, defaultValue, error } = useField(name);



  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <div id='input'>
      <div className='form-group'>
        <input
          className='form-control'
          defaultValue={defaultValue}
          ref={inputRef}
          {...rest}
        />
        <img className="icon" src={icon} alt="" />
        {error && <span className='error'> {error}</span>}
      </div>
    </div>
  );
};

export default Input;
