import React, {useRef} from 'react';

import Input from '../Input';
import InputMask from '../InputMask';
import InputSelect from '../InputSelect';

import { Form } from '@unform/web';
import * as Yup from 'yup';

import '../../Styles/Components/_contact-card.scss';

import Tel from '../../assets/images/contact-card/Tel.svg';
import tellIcon from '../../assets/images/contact-card/tell-icon.svg';
import locaIcon from '../../assets/images/contact-card/loca-icon.svg';
import clockIcon from '../../assets/images/contact-card/clock-icon.svg';
import personIcon from '../../assets/images/contact-card/person-icon.svg';
import emaiIcon from '../../assets/images/contact-card/enve-icon.svg';
import ToastAnimated, { showToast } from './../Toast';




const Contact = () =>{
  const formRef = useRef(null);

  const hours = [
    { value: "", label: "" },
    { value: "13:30", label: "13:30" },
    { value: "14:00", label: "14:00" },
    { value: "16:00", label: "16:00" }
  ];
  const unitys = [
    { value: "", label: "" },
    { value: "Salvador", label: "Salvador" },
    { value: "Aracaju", label: "Aracaju" },
    { value: "Belo Horizonte", label: "Belo Horizonte" }
  ];

  async function handleSubmit(data, {reset}) {
    try{
      const schema = Yup.object().shape({
        name: Yup.string()
        .required("campo obrigatório"),
        email: Yup.string()
          .required('campo obrigatório')
          .email('digite um E-mail válido'),
        telefone: Yup.string()
        .required("campo obrigatório"),
        hours: Yup.string()
        .required("campo obrigatório"),
        unity: Yup.string()
        .required("campo obrigatório"),
          
      })   

      await schema.validate(data, {
        abortEarly: false,
      })
      showToast({ type: "success", message: "Teste agendado com sucesso. Apenas um teste de validação!" });
      console.log(data);         
      formRef.current.setErrors({});
      reset();

    } catch(err){
      if(err instanceof Yup.ValidationError){
        console.log(err);
        const errorMessages = {};
        
        err.inner.forEach(error => {
          errorMessages[error.path] = error.message;
        })
        formRef.current.setErrors(errorMessages);
      }else{
        showToast({ type: "error", message: "Ocorreu algum erro ao agendar. Apenas um teste de validação!" });
        console.log(err)
      }
    }
  }

  return(
    <div id="contact">
      <ToastAnimated />
      <div className="contact-card">
        <div className="request">
          <img src={Tel} alt="" />
          <h4><span>solicite um</span> <br /> CONTATO</h4>
        </div>
        <Form ref={formRef} className="form" onSubmit={handleSubmit} >
          <Input
            name="name"
            placeholder="Nome"
            icon={personIcon}
          />
          <Input
            name="email"
            placeholder="E-mail"
            icon={emaiIcon}
          />
          <InputMask 
            name="telefone"
            placeholder="Telefone"
            mask="(79) 99999-9999"
            icon={tellIcon}
          />
          <InputSelect 
            name="hours"
            placeholder="Escolha o horário"
            options={hours}
            icon={clockIcon}
          />
          <InputSelect 
            name="unity"
            placeholder="Escolha a unidade"
            options={unitys}
            icon={locaIcon}
          />
          <button className="button" type="submit">
            AGENDAR TEST DRIVE
          </button>
        </Form>
      </div>
    </div>
  );
}

export default Contact;