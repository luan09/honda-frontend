import React, { useEffect , useState } from 'react';

import Civic from '../../assets/images/home/section2/Civic.png';
import CivicName from '../../assets/images/home/section2/Civic-name.svg';

import '../../Styles/Home/_section-2.scss';


const Section2 = () =>{

  const [flags, setFlags] = useState(true);

  const showFlags = () => {
     if (window.innerWidth >= 768) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="section-2">
      {flags && 
        <section className="Civic-images">
          <div className="civic"><img src={Civic} alt ="" /></div>
          <div className="civic-name"> <img  src={CivicName} alt="" /></div>
        </section> 
      }
    </div>
  );
}

export default Section2;