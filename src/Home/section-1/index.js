import React from 'react';

import logo from '../../assets/images/footer/logo.png';

import '../../Styles/Home/_section-1.scss';


const Section1 = () =>{
  return(
    <div id="section-1">
      <section className="logo-atendimento">
        <img className="logo" src={logo} alt ="" />

        <div className="central">
          <h5>CENTRAL DE ATENDIMENTO</h5>
          <h4>(31) 3123-1234</h4>
        </div>
      </section>
    </div>
  );
}

export default Section1;