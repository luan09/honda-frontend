import React, { useState, useEffect } from 'react';

import '../../Styles/Home/_section-4.scss';

import juntos from '../../assets/images/home/section4/juntosi1.png';
import carPart1 from '../../assets/images/home/section4/car-part-1.png';
import carPart2 from '../../assets/images/home/section4/car-part-2.png';
import carPart3 from '../../assets/images/home/section4/car-part-3.png';


const Section4 = () =>{
  const [flags, setFlags] = useState(true);

  const showFlags = () => {
     if (window.innerWidth >= 768) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="section-4">
      <section className="choose-photo">
        <h1>A EMOÇÃO DE DIRIGIR LEVA VOCÊ MAIS LONGE</h1>
        <div className="pictures-cars">
          {!flags &&  
            <div className="car-parts">
            <img src={carPart1} alt="" />
            <img src={carPart2} alt="" />
            <img src={carPart3} alt="" />
          </div>
          }
          <div className="picture-juntos">
            <img src={juntos} alt="" />
          </div>
        </div>
        <h2>
          O novo Civic Geração 10 traz um design único mudando a categoria de sedãs para sempre. 
          Ele garante a melhor dirigibilidade com o máximo em sofisticação e conforto, atribuído 
          por seu design interno e diferenciada ergonomia.
        </h2>
      </section>
    </div>
  );
}

export default Section4;