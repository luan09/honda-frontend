import exchange from '../../assets/images/home/section6/01-CAMBIO_AUTOMATICO.png';
import direction from '../../assets/images/home/section6/07-DIRECAO_ELETRICA.png';
import glasses from '../../assets/images/home/section6/08-VIDROS_ELETRICOS.png';


const data = [
  {
    id:1,
    picture: direction,
    title:"Direção com Assistência",
    subtitle:"Sistema de estabilidade reforça automaticamente o retorno da direção, auxiliando o motorista em situações onde o carro está ameaçado de instabilidade."
  },
  {
    id:2,
    picture: glasses,
    title:"Vidros elétricos com subida automática",
    subtitle:"Todas as portas possuem vidros elétricos com a função “um toque” e sistema de segurança antiesmagamento."
  },
  {
    id:3,
    picture: exchange,
    title:"Câmbio CVT com Paddle Shift",
    subtitle:"O câmbio com transmissão CVT de 7 velocidade com Paddle Shift (aletas) proporciona conforto ao trocar de marchas."
  },
  {
    id:4,
    picture: glasses,
    title:"Vidros elétricos com subida automática",
    subtitle:"Todas as portas possuem vidros elétricos com a função “um toque” e sistema de segurança antiesmagamento."
  },
  {
    id:5,
    picture: direction,
    title:"Direção com Assistência",
    subtitle:"Sistema de estabilidade reforça automaticamente o retorno da direção, auxiliando o motorista em situações onde o carro está ameaçado de instabilidade."
  },
  {
    id:6,
    picture: exchange,
    title:"Câmbio CVT com Paddle Shift",
    subtitle:"O câmbio com transmissão CVT de 7 velocidade com Paddle Shift (aletas) proporciona conforto ao trocar de marchas."
  },
]

export default data;