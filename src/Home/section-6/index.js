import React, { useState, useEffect } from 'react';
import Carousel from "react-elastic-carousel";

import '../../Styles/Home/_section-6.scss';

import data from './data';


const Section6 = () =>{

   const breakPoints = [
    { width: 550, itemsToShow: 1,},
    { width: 768, itemsToShow: 2 },
    { width: 900, itemsToShow: 3 , itemsToScroll: 1 },
  ];

  return(
    <div id="section-6">
      <section className="section-carousel">
        <div className="carousel">
          <Carousel 
            breakPoints={breakPoints}
          >
           {data.map((items) => { 
              return(
              <div className="container-carousel" key={items.id}>
                <div className="picture">
                  <img src={items.picture}  alt="" />
                </div>
                <h3>{items.title}</h3>
                <p>{items.subtitle}</p>
              </div>
              );
           })}
          </Carousel>
        </div>
      </section>
    </div>
  );
}

export default Section6;