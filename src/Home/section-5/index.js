import React, { useState, useEffect } from 'react';

import '../../Styles/Home/_section-5.scss';

import bg from '../../assets/images/home/section5/bg.png'

const Section5 = () =>{
  const [flags, setFlags] = useState(true);

  const showFlags = () => {
     if (window.innerWidth >= 768) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="section-5">
      <section className="new-civic">
        {flags &&
        <div className="grad">
        <h1>PREPARE-SE PARA ALGO REALMENTE NOVO</h1>
        <h5>O Novo Civic Geração 10 vem completo de série. Conheça a versão perfeita para você.</h5>
        </div>
        }
      </section>
    </div>
  );
}

export default Section5;