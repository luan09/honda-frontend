import React, { useState, useEffect } from 'react';
import { FiChevronDown } from "react-icons/fi";

import accord from '../../assets/images/home/section7/accord.png';
import city from '../../assets/images/home/section7/city.png';
import fit_2018 from '../../assets/images/home/section7/fit_2018.png';

import '../../Styles/Home/_section-7.scss';


const Section7 = () =>{
  const [click, setClick] = useState(false);
  const [flags, setFlags] = useState(true);

  const handleClick = () => setClick(!click);

  const showFlags = () => {
     if (window.innerWidth >= 768) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="section-7">
      <section className="section-ready">
        <div className="title-cards">
          <h1>PRONTO PARA TER UM HONDA?</h1>
          <h2>Conheça os últimos grandes lançamentos da honda.</h2>
          {flags ?
           <div className="cards">
            <div className={!click ? "container-card" : "container-expand"}>
            <div className="picture-seta" onClick={handleClick}>
              <img src={accord}  alt=""/>
              <FiChevronDown className={!click ? "seta" : "seta-rotate"} />
            </div>
            <p className={!click ? "hide" : "show"}>
              Novo Honda Accord: a combinação perfeita de três atributos exigidos pelo consumidor:
              elegância, luxo e alto desempenho.
            </p>
            </div>
            <button type="button" className="button">CONHEÇA MAIS</button>
           </div>
            :
            <div className="cards">
             <div className="container-card">
                <img src={accord}  alt=""/>
              <p>
                Novo Honda Accord: a combinação perfeita de três atributos exigidos pelo consumidor:
                elegância, luxo e alto desempenho.
              </p>
              <button type="button" className="button">CONHEÇA MAIS</button>
            </div>
            <div className="container-card">
                <img src={city}  alt=""/>
              <p>
                Novo Honda Accord: a combinação perfeita de três atributos exigidos pelo consumidor:
                elegância, luxo e alto desempenho.
              </p>
              <button type="button" className="button">CONHEÇA MAIS</button>
            </div>
            <div className="container-card">
                <img src={fit_2018}  alt=""/>
              <p>
                Novo Honda Accord: a combinação perfeita de três atributos exigidos pelo consumidor:
                elegância, luxo e alto desempenho.
              </p>
              <button type="button" className="button">CONHEÇA MAIS</button>
            </div>
            </div>
            }
          </div>
      </section>
    </div>
  );
}

export default Section7;