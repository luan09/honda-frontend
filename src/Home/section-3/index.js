import React from 'react';
import Contact from '../../Components/contact-card';

import '../../Styles/Home/_section-3.scss';


const Section3 = () =>{
  return(
    <div id="section-3">
      <Contact />
    </div>
  );
}

export default Section3;