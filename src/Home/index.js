import React, { useState, useEffect } from 'react';

import Section1 from './section-1';
import Section2 from './section-2';
import Section3 from './section-3';
import Section4 from './section-4';
import Section5 from './section-5';
import Section6 from './section-6';
import Section7 from './section-7';
import Section8 from './section-8';

import Contact from '../Components/contact-card';

import "./styles.scss";


const Home = () =>{

  const [flags, setFlags] = useState(true);

  const showFlags = () => {
     if (window.innerWidth >= 768) {
       setFlags(false);
     } else {
       setFlags(true);
     }
   };
 
   useEffect(() => {
     showFlags();
   }, []);
 
   window.addEventListener("resize", showFlags);

  return(
    <div id="home">
      <Section1 />
      <Section2 />
      {flags ? <Section3 />  : <Contact /> }
      <Section4 />
      <Section5 />
      <Section6 />
      <Section7 />
      <Section8 />
    </div>
  );
}

export default Home;