import React from 'react';

import '../../Styles/Home/_section-8.scss';

import face from '../../assets/images/home/section8/face.svg';
import insta from '../../assets/images/home/section8/insta.svg';
import loca from '../../assets/images/home/section8/loca.svg';
import twitter from '../../assets/images/home/section8/twitter.svg';



const Section8 = () =>{
  // const [flags, setFlags] = useState(true);

  // const showFlags = () => {
  //    if (window.innerWidth >= 768) {
  //      setFlags(false);
  //    } else {
  //      setFlags(true);
  //    }
  //  };
 
  //  useEffect(() => {
  //    showFlags();
  //  }, []);
 
  //  window.addEventListener("resize", showFlags);

  return(
    <div id="section-8">
      <section className="social-networks">
       <div className="cards">
          <div className="find">
            <img src={loca} alt=""/>
            <h4> <span>Encontre uma</span> <br /> CONCESSIONÁRIA</h4>
          </div>
          <div className="central">
            <h5>CENTRAL DE ATENDIMENTO</h5>
            <h4>(31) 3123-1234</h4>
          </div>
       </div>
       <h1>SIGA A BANZAI NAS REDES SOCIAIS</h1>
       <div className="socials">
         <div className="border">
          <a href="https://www.facebook.com/BanzaiHonda" target="_blank"rel="noreferrer noopener"> 
            <img src={face} alt="facebook" />
          </a>
          </div>
          <div className="border">
          <a href="https://www.instagram.com/banzaihonda_bh" target="_blank"rel="noreferrer noopener"> 
            <img src={insta} alt="instagram" />
          </a>
          </div>
          <div className="border">
          <a href="https://twitter.com/banzaihonda" target="_blank"rel="noreferrer noopener"> 
            <img src={twitter} alt="twitter" />
          </a>
          </div>
       </div>
      </section>
    </div>
  );
}

export default Section8;